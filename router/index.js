import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    component: resolve => require(['@/components/HelloWorld.vue'], resolve),
    meat: {
      title: 'HelloWorld'
    }
  },
  {
    path: '/test',
    component: resolve => require(['@/components/test.vue'], resolve),
    meat: {
      title: 'Test'
    }
  },
  {
    path: '/login',
    component: resolve => require(['@/components/home/login.vue'], resolve),
    meat: {
      title: 'Test'
    }
  }
]
export default new VueRouter({
  routes,
  mode: 'history'
})
